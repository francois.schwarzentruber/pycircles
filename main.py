import pygame
from game import Game


def initPyGame():
    pygame.init()
    pygame.mixer.init()
    pygame.font.init()
    pygame.display.set_caption('Circles')
        
def quitHandler():
    for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit();
    
def main():
    initPyGame()
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode((800, 600))
    game = Game()   

    while True:
        quitHandler()
        game.keysHandler(pygame.key.get_pressed())
        game.draw(screen);
        pygame.display.flip()
        clock.tick(60)
    
if __name__ == '__main__':
    main()

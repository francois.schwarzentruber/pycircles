#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  4 14:24:49 2019

@author: fschwarz
"""

import pygame

def play(name):
    pygame.mixer.music.load("music/" + name)
    pygame.mixer.music.play(-1)
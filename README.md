# pycircles

This repository contains a video game where everything is displayed with circles.
It can be used a teaching material to learn Python.

![alt text](pycircles.gif)
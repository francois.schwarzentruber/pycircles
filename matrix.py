#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 11:43:42 2019

@author: fschwarz
"""

from pyrr import Vector3, Matrix44, Matrix33, vector, vector3
from math import sin, cos

def lookAtMatrix(position, target, world_up):
        # 1.Position = known
        # 2.Calculate cameraDirection
        zaxis = vector.normalise(position - target)
        # 3.Get positive right axis vector
        xaxis = vector.normalise(vector3.cross(vector.normalise(world_up), zaxis))
        # 4.Calculate the camera up vector
        yaxis = vector3.cross(zaxis, xaxis)

        # create translation and rotation matrix
        translation = Matrix44.identity()
        translation[3][0] = -position.x
        translation[3][1] = -position.y
        translation[3][2] = -position.z

        rotation = Matrix44.identity()
        rotation[0][0] = xaxis[0]
        rotation[1][0] = xaxis[1]
        rotation[2][0] = xaxis[2]
        rotation[0][1] = yaxis[0]
        rotation[1][1] = yaxis[1]
        rotation[2][1] = yaxis[2]
        rotation[0][2] = zaxis[0]
        rotation[1][2] = zaxis[1]
        rotation[2][2] = zaxis[2]

        return translation * rotation
    
    
    
    
    
    



def rotationMatrix(α, β, γ):
    """
    rotation matrix of α, β, γ radians around x, y, z axes (respectively)
    """
    sα, cα = sin(α), cos(α)
    sβ, cβ = sin(β), cos(β)
    sγ, cγ = sin(γ), cos(γ)
    return Matrix33([
        [cβ*cγ, -cβ*sγ, sβ],
        [cα*sγ + sα*sβ*cγ, cα*cγ - sγ*sα*sβ, -cβ*sα],
        [sγ*sα - cα*sβ*cγ, cα*sγ*sβ + sα*cγ, cα*cβ]])

    
    
def vectorDir(r, α, β, γ):
    return rotationMatrix(α, β, γ) * Vector3([r, 0, 0])
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 17:47:21 2019

@author: fschwarz
"""


from sceneobject import SceneObject
from ball import Ball
from pyrr import Vector3
import random

CIRCLIFE = (192, 192, 255, 192)
CIRCLIFEINNER = (128, 128, 255)

class Circlife(SceneObject):
    def __init__(self, pos):
        super().__init__()
        self.pos = pos
        self.body = Ball(Vector3([pos.x, pos.y, pos.z]), 32, CIRCLIFE)
        self.body2 = Ball(Vector3([pos.x, pos.y, pos.z]), 32, CIRCLIFEINNER)
        self.live()
        self.addBall(self.body)
        self.addBall(self.body2)
        
        
    def live(self):
        self.body.c = self.pos
        self.body2.r = random.randint(0, 32)
        
    def collisionEffect(self, ball, hero):
        hero.addCirclife()
        
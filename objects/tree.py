#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 13:35:26 2019

@author: fschwarz
"""


from sceneobject import SceneObject
from ball import Ball
from pyrr import Vector3
from colors import *
import matrix
import random
import math

NB_APPLES = 3

class Tree(SceneObject):
        def __init__(self, pos, h, r):
            super().__init__()
            self.h = h + r
            self.r = r
            self.pos = pos;
            self.leaves = Ball(Vector3([pos.x, pos.y, pos.z + h]), r, getRandomGreenTree()) 
            self.apples = []
            self.addBall(self.leaves)

            for i in range(h-24, 0, -24):
                     self.addBall(Ball(Vector3([pos.x, pos.y, pos.z + i]), 16, BROWN))
            
            for i in range(0, NB_APPLES):
                    v = matrix.rotationMatrix(2*math.pi*random.random(), 2*math.pi*random.random(), 2*math.pi*random.random()) * Vector3([r * 3 / 4, 0, 0])
                    npos = Vector3([pos.x, pos.y, pos.z + h]) + Vector3([v.x, v.y, v.z])
                    apple = Ball(npos, 8, RED)
                    self.apples.append(apple)
                    self.addBall(apple)
            
        def live(self):
            self.leaves.r = random.randint(self.r - 2, self.r + 2)
            for apple in self.apples:
                apple.c = self.leaves.c + matrix.rotationMatrix(0, 0, 0.1) * (apple.c - self.leaves.c)
            
            
        
            
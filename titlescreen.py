#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 19:07:14 2019

@author: fschwarz
"""

from state import State
from scene import Scene
import pygame
from colors import *
import math
import music

class TitleScreen(State):
    
    def __init__(self):
        super().__init__()
        music.play("Circles_-_titlescreen.ogg")
        
    
    def drawCircle(self, screen, x, y, r):
            x = int(x)
            y = int(y)
            color = (YELLOW[0]  - y / 2, YELLOW[1], YELLOW[2] + y / 2)
            
            surface = pygame.Surface((2*r, 2*r), pygame.SRCALPHA, 32)
            pygame.draw.circle(surface, color, (r, r), r)
            screen.blit(surface, (x-r, y-r))
            del surface
           # pygame.draw.circle(screen, color, (x, y), r)
            if r > 3:
                pygame.draw.circle(screen, BLACK, (x, y), r, 3)
                
                
    def drawC(self, screen, x, y):
        self.drawCircle(screen, x + 64, y, 64)
        self.drawArc(screen, x, y, 0)
            
    def drawArc(self, screen, x, y, beginangle):
     for i in range(0, 9):
            angle = beginangle + i *math.pi/8
            self.drawCircle(screen, x + 64 - 48*math.sin(angle), y - 48*math.cos(angle), 16)
        
    def drawI(self, screen, x, y):
        for i in range(0, 4):
            self.drawCircle(screen, x + 64, y + 32 - 16*i, 16)
            
    def drawR(self, screen, x, y):
        self.drawCircle(screen, x + 32, y - 32, 32)
        for i in range(0, 4):
            self.drawCircle(screen, x + 16, y  + 16*i, 16)
        for i in range(0, 4):
            self.drawCircle(screen, x + 64 + 8*i, y  + 16*i, 16)
    
    def drawL(self, screen, x, y):
        for i in range(0, 8):
            self.drawCircle(screen, x + 64, y + 32 - 16*i, 16)

    def drawE(self, screen, x, y):
        self.drawCircle(screen, x + 32, y - 32, 32)        
        for i in range(0, 4):
            self.drawCircle(screen, x + 16, y  + 16*i, 16)
        for i in range(0, 4):
            self.drawCircle(screen, x + 16 + 16*i, y + 16*4, 16)
            
    def drawS(self, screen, x, y):
        self.drawArc(screen, x, y, 0)
        self.drawArc(screen, x, y + 64, math.pi)

    def draw(self, screen):
        screen.fill(BLUE)
        
        BEGIN = 60
        ECART = 96
        Y = 200
        self.drawC(screen, BEGIN, Y)
        self.drawI(screen, BEGIN+ECART, Y)
        self.drawR(screen, BEGIN+ECART*2, Y)
        self.drawC(screen, BEGIN+ECART*3, Y)
        self.drawL(screen, BEGIN+ECART*4, Y)
        self.drawE(screen, BEGIN+ECART*5, Y)
        self.drawS(screen, BEGIN+ECART*6, Y)
        drawText(screen, "Press enter", 300, 400, 64)
        
                
    def keysHandler(self, keys):
        if(keys[pygame.K_RETURN]):
            self.setNextState(Scene())

def drawText(screen, string, x, y, size):
        myfont = pygame.font.SysFont('Comic Sans MS', size)
        textsurface = myfont.render(string, False, (0, 0, 0))
        screen.blit(textsurface,(x,y))            